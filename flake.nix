{
 description = "Home Manager configuration of vamshi";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    j-thoughts = {
        url = "gitlab:vamshi5070/j-thoughts?rev=61b239a22b85d432d4770762fdd8fed835a3d0c4";
        inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = inputs@{ nixpkgs, home-manager,j-thoughts, ... }:
    let
      system = "aarch64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      # my-package = j-thoughts.packages.${system}.j-thoughts;
    in {
      homeConfigurations."vamshi" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [ ./home.nix
                    {
                      home.packages = [
                        # my-package.legacyPackages.j-thoughts
                        # my-package
#                        "gitlab:vamshi5070/j-thoughts"
inputs.j-thoughts.defaultPackage.${system}
#packages.aarch64-linux.j-thoughts

                      ];
                    }
                  ];

        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };
    };
}
